## Bowling challenge

### Setup

Run `npm install` and then `npm start`. Open `localhost:3000` in your browser

### Tech

React + Redux + Webpack + Jest
I used a boilerplate project for setup.

### Testing

run `npm test`

### Todos

- if there was more time I would split up the `UPDATE_SCORE` action handler into smaller pieces
- add more unit tests for the reducers
- add unit tests for components using enzyme
