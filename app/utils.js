export function arrayFromNumber(count) {
  return [ ...Array(count).keys() ];
}


export function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}


export function randomSubset(array) {
  array = [ ...array ];
  const removeCount = getRandomInt(0, array.length - 1);
  const removedItems = [];

  for (let i = 0; i <= removeCount; i++) {
    const indexToRemove = getRandomInt(0, array.length - 1);
    removedItems.push(array[indexToRemove]);
    array.splice(indexToRemove, 1);
  }

  return removedItems;
}


export function calculateScore(excludedPins = []) {
  let visiblePins = arrayFromNumber(10);
  if (excludedPins.length) visiblePins = visiblePins.filter(pin => !excludedPins.includes(pin));
  return [ ...randomSubset(visiblePins), ...excludedPins ];
}


export function immutableReplace(array, index, item) {
  array = [ ...array ];
  array.splice(index, 1, item);
  return array;
}
