import * as types from './types';


export function updateScore(newHiddenPins) {
  return {
    type: types.UPDATE_SCORE,
    payload: { newHiddenPins }
  };
}


export function switchPlayers() {
  return { type: types.SWITCH_PLAYERS };
}


export function initGame(playerCount) {
  return {
    type: types.INIT_GAME,
    payload: { playerCount }
  };
}
