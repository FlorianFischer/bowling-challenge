import React from 'react';
import PropTypes from 'prop-types';
import Frame from '../components/Frame';

const ScoreBoard = ({ playerIds, playersById }) => {
  return (
    <div className="score-board">
      {playerIds.map(id => {
        const player = playersById[id];

        return (
          <div className="player-score" key={ id }>
            {player.frames.map((frame, index) => {
              return <Frame key={ index } frame={ frame } />;
            })}
          </div>
        );
      })}
    </div>
  );
};


ScoreBoard.propTypes = {
  playerIds: PropTypes.array.isRequired,
  playersById: PropTypes.object.isRequired
};


export default ScoreBoard;
