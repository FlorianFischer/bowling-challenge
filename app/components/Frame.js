import React from 'react';
import PropTypes from 'prop-types';


const Frame = ({ frame }) => {
  const { firstScore, secondScore, frameScore, isStrike, isSpare } = frame;

  return (
    <div className="frame">
      <div className="first-score">{ isStrike ? 'X' : firstScore }</div>
      <div className="second-score">{ isSpare ? '/' : secondScore }</div>
      <div className="frame-score">{ frameScore }</div>
    </div>
  );
};


Frame.propTypes = {
  frame: PropTypes.object.isRequired
};


export default Frame;
