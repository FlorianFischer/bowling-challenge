import React from 'react';
import PropTypes from 'prop-types';


const Pin = ({ isVisible, index }) => {
  return <div className={ `pin position-${index} ${isVisible ? 'is-visible' : ''}` }></div>;
};


Pin.propTypes = {
  isVisible: PropTypes.bool.isRequired,
  index: PropTypes.number.isRequired
};


export default Pin;
