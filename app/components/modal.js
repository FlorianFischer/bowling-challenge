import React from 'react';
import PropTypes from 'prop-types';


const Modal = ({ children }) => {
  return (
    <div className="backdrop">
      <div className="modal">
        { children }
      </div>
    </div>
  );
};


Modal.propTypes = {
  children: PropTypes.node,
};


export default Modal;
