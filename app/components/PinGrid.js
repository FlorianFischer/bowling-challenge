import React from 'react';
import PropTypes from 'prop-types';
import Pin from '../components/Pin';
import { arrayFromNumber } from '../utils';


const PinGrid = ({ hiddenPins }) => {
  const totalRows = 4;
  let pinCount = 0;

  return (
    <div>
      {arrayFromNumber(totalRows).map(rowIndex => {
        return (
          <div className="row" key={ rowIndex }>
            {arrayFromNumber(totalRows - rowIndex).map(pinIndex => {
              const totalIndex = pinCount;
              const isVisible = hiddenPins.indexOf(totalIndex) === -1;
              pinCount++;

              return <Pin key={ pinIndex } index={ totalIndex } isVisible={ isVisible } />;
            })}
          </div>
        );
      })}
    </div>
  );
};


PinGrid.propTypes = {
  hiddenPins: PropTypes.array.isRequired
};


export default PinGrid;
