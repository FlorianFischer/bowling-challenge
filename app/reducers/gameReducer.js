import * as types from '../actions/types';
import { arrayFromNumber, immutableReplace } from '../utils';
import shortid from 'shortid';


const initialState = {
  currentPlayer: null,
  frameCount: 0,
  availableRolls: 2,
  hiddenPins: [],
  playerIds: [],
  playersById: {}
};


const gameReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.INIT_GAME: {
      const { playerCount } = action.payload;
      const playerIds = arrayFromNumber(Number(playerCount)).map(() => shortid.generate());
      const playersById = {};
      const frames = arrayFromNumber(10).map(() => {
        return {
          firstScore: null,
          secondScore: null,
          frameScore: null,
          isStrike: false,
          isSpare: false
        };
      });

      playerIds.forEach(id => {
        playersById[id] = {
          totalScore: 0,
          frames: [ ...frames ]
        };
      });

      return {
        ...state,
        currentPlayer: playerIds[0],
        playersById,
        playerIds
      };
    }

    case types.UPDATE_SCORE: {
      const { newHiddenPins } = action.payload;
      const { currentPlayer, frameCount, playersById, availableRolls } = state;
      const score = newHiddenPins.length;
      const isBonus = score === 10;
      const isFirstRoll = availableRolls === 2;
      const isSecondRoll = availableRolls === 1;
      let frames = playersById[currentPlayer].frames;
      let newPreviousFrame;

      const previousFrame = frameCount ? frames[frameCount - 1] : null;
      const { firstScore, secondScore, frameScore } = frames[frameCount];

      const newCurrentFrame = {
        firstScore: firstScore ? firstScore : score,
        secondScore: isSecondRoll ? score - firstScore : null,
        frameScore: isBonus || isFirstRoll ? null : score,
        isStrike: isFirstRoll && isBonus,
        isSpare: isSecondRoll && isBonus
      };

      if (previousFrame && previousFrame.isStrike && isSecondRoll) {
        const newCurrentFrameScore = newCurrentFrame.isStrike ? 10 : newCurrentFrame.firstScore + newCurrentFrame.secondScore;
        const newPreviousFrameScore = 10 + newCurrentFrameScore;
        newPreviousFrame = {
          ...previousFrame,
          frameScore: newPreviousFrameScore
        };
      }

      if (previousFrame && previousFrame.isSpare && isFirstRoll) {
        newPreviousFrame = {
          ...previousFrame,
          frameScore: 10 + newCurrentFrame.firstScore
        };
      }

      frames = immutableReplace(frames, frameCount, newCurrentFrame);
      if (newPreviousFrame) frames = immutableReplace(frames, frameCount - 1, newPreviousFrame);

      return {
        ...state,
        availableRolls: newCurrentFrame.isStrike ? 0 : availableRolls - 1,
        hiddenPins: [ ...newHiddenPins ],
        playersById: {
          ...state.playersById,
          [currentPlayer]: {
            ...state.playersById[currentPlayer],
            frames
          }
        }
      };
    }


    case types.SWITCH_PLAYERS: {
      const { currentPlayer, frameCount, availableRolls, playerIds } = state;
      const currentPlayerIndex = playerIds.indexOf(currentPlayer);
      const isLastPlayer = currentPlayerIndex === playerIds.length - 1;
      let nextPlayer;

      if (isLastPlayer) nextPlayer = playerIds[0];
      else nextPlayer = playerIds[currentPlayerIndex + 1];

      return {
        ...state,
        currentPlayer: nextPlayer,
        availableRolls: 2,
        frameCount: isLastPlayer ? frameCount + 1 : frameCount,
        hiddenPins: []
      };
    }

    default:
      return state;
  }
};


export default gameReducer;
