import React from 'react';
import { Route, Switch } from 'react-router-dom';
import GameContainer from './containers/GameContainer';

export default (
	<Switch>
		<Route exact path="/" component={ GameContainer } />
	</Switch>
);
