import PropTypes from 'prop-types';
import React from 'react';
import { connect } from 'react-redux';
import PinGrid from '../components/PinGrid';
import Modal from '../components/Modal';
import ScoreBoard from '../components/ScoreBoard';
import { calculateScore } from '../utils';
import * as gameActions from '../actions/gameActions';


class GameContainer extends React.Component {
  static state = {
    playerCount: null
  }


  handleRoll() {
    const { hiddenPins, updateScore } = this.props;
    updateScore(calculateScore(hiddenPins));
  }


  render() {
    const {
      hiddenPins,
      currentPlayer,
      initialize,
      frameCount,
      playerIds,
      availableRolls,
      switchPlayers,
      playersById
    } = this.props;

    return (
      <div className="container">
        <ScoreBoard
         playerIds={ playerIds }
         playersById={ playersById } />

        <div className="game-container">
          <PinGrid hiddenPins={ hiddenPins } />
        </div>

        <button
         onClick={ this.handleRoll.bind(this)  }
         type="button">
         Roll!
        </button>

        {!currentPlayer && (
          <Modal>
            <div>Please enter the number of players.</div>
            <input
             type="number"
             onChange={ event => this.setState({ playerCount: event.target.value }) } />
            <button
             type="button"
             onClick={ () => initialize(this.state.playerCount) }>
             Lets go
            </button>
          </Modal>
        )}

        {availableRolls === 0 && (
          <Modal>
            <div>Next player, your turn!</div>
            <button
             type="button"
             onClick={ () => switchPlayers() }>
             Lets go
            </button>
          </Modal>
        )}
      </div>
    );
  }
}


GameContainer.propTypes = {
  updateScore: PropTypes.func.isRequired,
  initialize: PropTypes.func.isRequired,
  switchPlayers: PropTypes.func.isRequired,
  frameCount: PropTypes.number.isRequired,
  currentPlayer: PropTypes.string,
  hiddenPins: PropTypes.array.isRequired,
  playerIds: PropTypes.array.isRequired,
  playersById: PropTypes.object.isRequired,
  availableRolls: PropTypes.number.isRequired
};


const mapStateToProps = state => {
  return {
    currentPlayer: state.game.currentPlayer,
    hiddenPins: state.game.hiddenPins,
    frameCount: state.game.frameCount,
    playerIds: state.game.playerIds,
    playersById: state.game.playersById,
    availableRolls: state.game.availableRolls
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    initialize: playerCount => dispatch(gameActions.initGame(playerCount)),
    updateScore: score => dispatch(gameActions.updateScore(score)),
    switchPlayers: () => dispatch(gameActions.switchPlayers())
  };
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GameContainer);
