import * as actions from '../actions/gameActions';
import * as types from '../actions/types';


describe('actions', () => {
  it('should create action update score', () => {
    const newHiddenPins = [1, 2, 3];
    const expectedAction = {
      type: types.UPDATE_SCORE,
      payload: { newHiddenPins }
    };

    expect(actions.updateScore(newHiddenPins)).toEqual(expectedAction);
  });


  it('should create an action to switch players', () => {
    const expectedAction = { type: types.SWITCH_PLAYERS };
    expect(actions.switchPlayers()).toEqual(expectedAction);
  });


  it('should create an action to initialize the game', () => {
    const playerCount = 2;
    const expectedAction = {
      type: types.INIT_GAME,
      payload: { playerCount }
    };

    expect(actions.initGame(2)).toEqual(expectedAction);
  });
});
