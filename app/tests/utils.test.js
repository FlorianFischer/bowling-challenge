import * as utils from '../utils';


describe('utils', () => {
  it('create an array of numbers', () => {
    expect(utils.arrayFromNumber(3)).toEqual([0, 1, 2]);
  });


  it('should return an integer in range', () => {
    const result = utils.getRandomInt(0, 4);

    expect(typeof result).toEqual('number');
    expect(result).toBeGreaterThanOrEqual(0)
    expect(result).toBeLessThanOrEqual(4);
  });


  it('should return a random subset of the input', () => {
    const input = [0, 1, 2, 3, 4];
    const result = utils.randomSubset(input);

    expect(result.length).toBeLessThanOrEqual(input.length);
    result.forEach(number => expect(input.indexOf(number) > -1));
  });


  it('should add numbers to an array in range 0 to 9 that are not in input array', () => {
    const input = [0, 1, 2];
    const result = utils.calculateScore(input);

    result.forEach(number => {
      expect(input.indexOf(number) > -1)
      expect(number).toBeLessThanOrEqual(9);
      expect(number).toBeGreaterThanOrEqual(0);
    });
    expect(result.length).toBeGreaterThanOrEqual(input.length);
  });
});
