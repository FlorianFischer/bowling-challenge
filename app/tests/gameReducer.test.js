import gameReducer from '../reducers/gameReducer';
import { initGame, updateScore, switchPlayers } from '../actions/gameActions';


function testFrame(firstScore, secondScore, frameScore, isStrike, isSpare, frame) {
  expect(frame.firstScore).toEqual(firstScore);
  expect(frame.secondScore).toEqual(secondScore);
  expect(frame.frameScore).toEqual(frameScore);
  expect(frame.isStrike).toEqual(isStrike);
  expect(frame.isSpare).toEqual(isSpare);
}


describe('gameReducer', () => {
  it('should initialize game state', () => {
    const action = initGame(2);
    const { currentPlayer, playerIds, playersById } = gameReducer(undefined, action);

    expect(currentPlayer.length).toBeGreaterThan(0);
    expect(playerIds).toContain(currentPlayer);
    expect(playerIds.length).toEqual(2);
    expect(Object.keys(playersById).length).toEqual(2);
    playerIds.forEach(id => expect(playersById[id].frames.length).toEqual(10));
  });


  it('should add the correct score on non bonus roll', () => {
    const initialScore = [1, 2, 3];
    const initialState = gameReducer(undefined, initGame(1));
    let updatedState = gameReducer(initialState, updateScore(initialScore));
    let { hiddenPins, availableRolls, playersById, playerIds } = updatedState;
    let currentFrame = playersById[playerIds[0]].frames[0];

    expect(availableRolls).toEqual(1);
    testFrame(3, null, null, false, false, currentFrame);
    hiddenPins.forEach(pin => expect(initialScore).toContain(pin));

    const secondScore = [1, 2, 3, 4, 5];
    updatedState = gameReducer(updatedState, updateScore(secondScore));
    currentFrame = updatedState.playersById[playerIds[0]].frames[0];

    expect(updatedState.availableRolls).toEqual(0);
    testFrame(3, 2, 5, false, false, currentFrame);
    updatedState.hiddenPins.forEach(pin => expect(secondScore).toContain(pin));
  });


  it('should skip the second roll on strike', () => {
    const initialScore = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const initialState = gameReducer(undefined, initGame(1));
    const updatedState = gameReducer(initialState, updateScore(initialScore));
    const { hiddenPins, availableRolls, playersById, playerIds } = updatedState;
    const currentFrame = playersById[playerIds[0]].frames[0];

    expect(availableRolls).toEqual(0);
    testFrame(10, null, null, true, false, currentFrame);
    hiddenPins.forEach(pin => expect(initialScore).toContain(pin));
  });


  it('should add the next two scores after a strike', () => {
    const initialScore = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
    const initialState = gameReducer(undefined, initGame(1));
    const player = initialState.playerIds[0];

    // first roll: strike
    let updatedState = gameReducer(initialState, updateScore(initialScore));

    // switch player (frame)
    updatedState = gameReducer(updatedState, switchPlayers())

    // second roll
    const firstScore = [0, 1, 2];
    updatedState = gameReducer(updatedState, updateScore(firstScore));
    let strikeFrame = updatedState.playersById[player].frames[0];
    testFrame(10, null, null, true, false, strikeFrame);

    // third roll
    const secondScore = [0, 1, 2, 3, 4, 5];
    updatedState = gameReducer(updatedState, updateScore(secondScore));
    strikeFrame = updatedState.playersById[player].frames[0];
    testFrame(10, null, 16, true, false, strikeFrame);
  });
});
